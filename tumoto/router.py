#from tumotoapp.views.UserCreateView import UserCreateView
from tumotoapp.views.viewsets import UserViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('user', UserViewset)

# localhost:8000/api/usuario/#
# GET, POST, PUT, DELETE
# list, retrive 
