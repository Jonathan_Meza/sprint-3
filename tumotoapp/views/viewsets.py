from rest_framework import viewsets
from tumotoapp import models
from tumotoapp.models.user import User
#from tumotoapp.serializers.accountSerializer import AccountSerializer
from tumotoapp import serializers
from rest_framework.response import Response
from rest_framework import status

class UserViewset(viewsets.ModelViewSet):
	queryset = models.user.User.objects.all()
	serializer_class = serializers.userSerializer.UserSerializer

	# def update(self, request, pk=None):
	# 	return Response({'mensaje': 'Se modificó satisfactoriamente!'})

	# def list(self, request):
	# 	return Response({'mensaje': 'Listado de Usuarios Creados en BD'})

	# def delete(self, request, pk=None):
	# 	user = self.get_queryset().filter(id=pk).first()
	# 	if user:
	# 		user.state = False
	# 		user.save()
	# 		return Response({'Los Datos del Usuario se han eliminado satisfactoriamente!'}, status=status.HTTP_200_OK)